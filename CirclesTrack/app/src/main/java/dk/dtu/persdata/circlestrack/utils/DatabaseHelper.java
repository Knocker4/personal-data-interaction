package dk.dtu.persdata.circlestrack.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.annimon.stream.Stream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.persdata.circlestrack.entities.Group;
import dk.dtu.persdata.circlestrack.entities.Person;

import static android.R.attr.id;

/**
 * Created by knocker4 on 4/11/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "CircleTracks.db";
    private static final int VERSION = 1;

    private static final String GROUPS_TABLE_NAME = "GROUPS";
//    private static final String GROUP_ID = "_id";
    private static final String GROUP_NAME = "name";
    private static final String GROUP_PEOPLE = "people";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + GROUPS_TABLE_NAME + "(" +
//                GROUP_ID + " INTEGER PRIMARY KEY, " +
                GROUP_NAME + " TEXT PRIMARY KEY, " +
                GROUP_PEOPLE + " TEXT)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void insertGroup(Group group) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
//        contentValues.put(GROUP_ID, );

        contentValues.put(GROUP_NAME, group.getTitle());
        JSONArray array = new JSONArray();
        Stream.of(group.getPersons())
              .map(Person::toJson)
              .forEach(array::put);
        contentValues.put(GROUP_PEOPLE, array.toString());
        db.insert(GROUPS_TABLE_NAME, null, contentValues);
    }

    public void insertGroups(List<Group> groupList) {
        Stream.of(groupList).forEach(this::insertGroup);
    }

    public boolean updateGroup(Group group) {
        if (this.getGroup(group.getTitle()) == null) {
            insertGroup(group);
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(GROUP_NAME, group.getTitle());
            JSONArray array = new JSONArray();
            Stream.of(group.getPersons())
                    .map(Person::toJson)
                    .forEach(array::put);
            contentValues.put(GROUP_PEOPLE, array.toString());
            db.update(GROUPS_TABLE_NAME, contentValues, GROUP_NAME + " = ? ", new String[] { group.getTitle() } );
        }
        return true;
    }

    public Group getGroup(String groupName) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + GROUPS_TABLE_NAME + " WHERE " + GROUP_NAME + " = ?",
                new String[]{groupName} );
        Group group = null;
        int nameI = cursor.getColumnIndex(GROUP_NAME);
        int peopleI = cursor.getColumnIndex(GROUP_PEOPLE);
        if (cursor.moveToFirst()) {
            String name = cursor.getString(nameI);
            List<Person> persons = new ArrayList<>();
            try {
                JSONArray array = new JSONArray(cursor.getString(peopleI));
                for (int i = 0; i < array.length(); i++) {
                    persons.add(new Person(new JSONObject(array.getString(i))));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            group = new Group(name, persons);
        }
        cursor.close();
        return group;
    }

    public List<Group> getAllGroups() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery( "SELECT * FROM " + GROUPS_TABLE_NAME, null );
        List<Group> groups = new ArrayList<>();
        int nameI = cursor.getColumnIndex(GROUP_NAME);
        int peopleI = cursor.getColumnIndex(GROUP_PEOPLE);
        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(nameI);
                List<Person> persons = new ArrayList<>();
                try {
                    JSONArray array = new JSONArray(cursor.getString(peopleI));
                    for (int i = 0; i < array.length(); i++) {
                        persons.add(new Person(new JSONObject(array.getString(i))));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                groups.add(new Group(name, persons));
            } while ((cursor.moveToNext()));
        }
        return groups;
    }

}
