package dk.dtu.persdata.circlestrack.entities;

import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by knocker4 on 4/11/17.
 */

public class Group {

    private String _title;
    private List<Person> _persons;

    public Group(String title) {
        _title = title;
        _persons = new ArrayList<>();
    }


    public Group(String title, List<Person> persons) {
        _title = title;
        _persons = persons;
    }

    public String getTitle() {
        return _title;
    }

    public void setPersons(List<Person> persons) {
        _persons = persons;
    }

    public List<Person> getPersons() {
        return _persons;
    }

    public void addPerson(Person person) {
        if (_persons != null)
            _persons = new ArrayList<>();
        _persons.add(person);
    }

    public int getInteractions() {
        int i = 0;
        for (Person person : _persons) {
            i += person.getEncounters();
        }
        return i;
    }

    @Override
    public String toString() {
        return _title + " members: " + _persons.size() + " interactions: " + getInteractions();
    }

    @Override
    public int hashCode() {
        return _title.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Group && ((Group) obj).getTitle().equals(_title);
    }
}
