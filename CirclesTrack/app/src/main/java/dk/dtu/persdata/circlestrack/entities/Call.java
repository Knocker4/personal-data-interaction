package dk.dtu.persdata.circlestrack.entities;

import android.provider.CallLog;

import java.util.Date;

/**
 * Created by knocker4 on 4/5/17.
 */

public class Call {

    private String _phoneNumber;
    private int _callType;
    private Date _callDate;


    public Call(String phoneNumber, int callType, Date callDate) {
        _phoneNumber = phoneNumber;
        _callType = callType;
        _callDate = callDate;
    }

    public String getPhoneNumber() {
        return _phoneNumber;
    }

    public int getCallType() {
        return _callType;
    }

    public Date getCallDate() {
        return _callDate;
    }

    @Override
    public String toString() {
        return "{"+_phoneNumber + " : " + _callDate.toString() +"}";
    }
}
