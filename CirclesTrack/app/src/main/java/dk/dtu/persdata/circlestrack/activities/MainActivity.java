package dk.dtu.persdata.circlestrack.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import dk.dtu.persdata.circlestrack.R;
import dk.dtu.persdata.circlestrack.entities.Call;
import dk.dtu.persdata.circlestrack.entities.Group;
import dk.dtu.persdata.circlestrack.entities.Person;
import dk.dtu.persdata.circlestrack.entities.Sms;
import dk.dtu.persdata.circlestrack.utils.ConnectionsHistoryHelper;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_READ_CONTACTS = 111;
    public static final int REQUEST_READ_CALL_LOG = 222;
    public static final int REQUEST_READ_SMS_LOG = 333;
    public static final int REQUEST_EDIT_GROUPS = 444;

    public static String EXTRA_MESSAGE = "sasa";
    private static final String TAG = "MAIN_ACTIVITY";
    private static final String SHARED_PREF_FIRST_LAUNCH = "FIRST_LAUNCH";

    private List<Person> _personList;
    private List<Call> _callList;
    private List<Sms> _smsList;
    private List<Group> _groups;
    private Group _selectedGroup;

    private FrameLayout _pieChartContainer;
    private FrameLayout _barChartContainer;
    private PieChart _pieChart;
    private HorizontalBarChart _barChart;
    private boolean firstLaunch;

    private ImageView _buttonDay;
    private ImageView _buttonWeek;
    private ImageView _buttonMonth;
    private ImageView _buttonYear;

    private ImageView _previouslySelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _buttonDay = (ImageView) findViewById(R.id.buttonDay);
        _buttonWeek = (ImageView) findViewById(R.id.buttonWeek);
        _buttonMonth = (ImageView) findViewById(R.id.buttonMonth);
        _buttonYear = (ImageView) findViewById(R.id.buttonYear);

        _pieChart = (PieChart) findViewById(R.id.pieChart);
        _barChart = (HorizontalBarChart) findViewById(R.id.barChart);

        SharedPreferences sharedPreferences = getSharedPreferences(
                getString(R.string.share_pref_title), MODE_PRIVATE);

        firstLaunch = sharedPreferences.getBoolean(SHARED_PREF_FIRST_LAUNCH, true);
//        firstLaunch = true;
        if (firstLaunch) {
            checkPermissionsAndStartSetup();
        }

        ConnectionsHistoryHelper.initiateGroups(this);
        setupControls();
        reloadHistory(ConnectionsHistoryHelper.SelectionType.YEAR);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_settings: {
                startActivityForResult(GroupsActivity.getActivityIntent(this, true), REQUEST_EDIT_GROUPS);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQUEST_EDIT_GROUPS) {
            _groups = ConnectionsHistoryHelper.getGroups(this);
            setupPieChart();
            SharedPreferences sharedPreferences = getSharedPreferences(
                    getString(R.string.share_pref_title),
                    MODE_PRIVATE
            );
            sharedPreferences.edit().putBoolean(SHARED_PREF_FIRST_LAUNCH, false).apply();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CALL_LOG: {
                _callList = ConnectionsHistoryHelper.getCallHistory(this);
                break;
            }
            case REQUEST_READ_CONTACTS: {
                _personList = ConnectionsHistoryHelper.getContacts(this);
                break;
            }
            case REQUEST_READ_SMS_LOG: {
                _smsList = ConnectionsHistoryHelper.getSMSDetails(this);
                break;
            }
        }
        checkPermissionsAndStartSetup();
    }

    private void checkPermissionsAndStartSetup() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS);
        } else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CALL_LOG},
                    REQUEST_READ_CALL_LOG);
        } else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_SMS},
                    REQUEST_READ_SMS_LOG);
        } else {
            startActivityForResult(GroupsActivity.getActivityIntent(this, false), REQUEST_EDIT_GROUPS);
        }
    }


    private void setupControls() {
        _buttonDay.setOnClickListener(v -> {
            if (!v.isSelected()) {
                reloadHistory(ConnectionsHistoryHelper.SelectionType.DAY);
                switchSelected(v);
                switchSelected(_previouslySelected);
                _previouslySelected = (ImageView) v;
            }
        });
        _buttonDay.setColorFilter(getResources().getColor(R.color.colorBottomIconNeutral));
        _buttonWeek.setOnClickListener(v -> {
            if (!v.isSelected()) {
                reloadHistory(ConnectionsHistoryHelper.SelectionType.WEEK);
                switchSelected(v);
                switchSelected(_previouslySelected);
                _previouslySelected = (ImageView) v;
            }
        });
        _buttonWeek.setColorFilter(getResources().getColor(R.color.colorBottomIconNeutral));
        _buttonMonth.setOnClickListener(v -> {
            if (!v.isSelected()) {
                reloadHistory(ConnectionsHistoryHelper.SelectionType.MONTH);
                switchSelected(v);
                switchSelected(_previouslySelected);
                _previouslySelected = (ImageView) v;
            }
        });
        _buttonMonth.setColorFilter(getResources().getColor(R.color.colorBottomIconNeutral));
        _buttonYear.setOnClickListener(v -> {
            if (!v.isSelected()) {
                reloadHistory(ConnectionsHistoryHelper.SelectionType.YEAR);
                switchSelected(v);
                switchSelected(_previouslySelected);
                _previouslySelected = (ImageView) v;
            }
        });
        switchSelected(_buttonYear);
        _previouslySelected = _buttonYear;
    }

    private void switchSelected(View v) {
        v.setSelected(!v.isSelected());
        ((ImageView) v).setColorFilter(
                v.isSelected() ?
                        getResources().getColor(R.color.colorBottomIconSelected) :
                        getResources().getColor(R.color.colorBottomIconNeutral));
    }

    private void reloadHistory(ConnectionsHistoryHelper.SelectionType selectionType) {
        ConnectionsHistoryHelper.resetEncounters();
        ConnectionsHistoryHelper.setSelectionType(selectionType);
        _groups = ConnectionsHistoryHelper.getGroups(this);
        _personList = ConnectionsHistoryHelper.getContacts(this);
        _callList = ConnectionsHistoryHelper.getCallHistory(this);
        _smsList = ConnectionsHistoryHelper.getSMSDetails(this);
        ConnectionsHistoryHelper.refreshGroups();
        setupPieChart();
    }

    private float percentage(float a, float b) {
        return ((float) a) / ((float) b) * 100;
    }


    private void setupBarChart(Group group) {
        if (group == null)
            return;
        float barWidth = 9f;
        float spaceForBar = 10f;
        List<BarEntry> entries = new ArrayList<>();
        List<Person> sorted = Stream.of(group.getPersons())
                .sorted((o1, o2) -> o1.getEncounters() - o2.getEncounters())
                .skip(group.getPersons().size() > 10 ? group.getPersons().size() - 10 : 0)
                .collect(Collectors.toList());
        System.out.println(sorted.size());
        if (sorted.size() < 10) {
            System.out.println("need to add rows");
            int rowsToAdd = (10-sorted.size());
            for (int i=0; i < rowsToAdd; i++) {
                System.out.println("adding row");
                sorted.add(0, new Person(-1, "", "", ""));
            }
        }
        System.out.println(sorted.size());

        for (int i = 0; i < sorted.size(); i++) {
            entries.add(new BarEntry(i * spaceForBar, sorted.get(i).getEncounters(), sorted.get(i)));
        }
        BarDataSet dataSet = new BarDataSet(entries, group.getTitle());
        dataSet.setDrawIcons(true);

        BarData data = new BarData(dataSet);
        data.setValueTextSize(14f);
        data.setValueFormatter((value, entry, dataSetIndex, viewPortHandler) -> "");
        data.setBarWidth(barWidth);
        data.setHighlightEnabled(false);

        XAxis xl = _barChart.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.TOP_INSIDE);
        xl.setTextSize(14f);
        xl.setDrawAxisLine(false);
        xl.setDrawGridLines(false);
        xl.setDrawLabels(true);
        xl.setGranularityEnabled(true);
        xl.setLabelCount(10);
        xl.setGranularity(spaceForBar);
        String valueFormat = "%s %.1f%%";
        xl.setValueFormatter((value, axis) -> {
            if (((int) (value / spaceForBar)) >= entries.size())
                return "";
            Person person = ((Person) entries.get((int) (value / spaceForBar)).getData());
            if (person.getId() == -1)
                return "";
            return String.format(
                    valueFormat,
                    person.getName(),
                    percentage(person.getEncounters(), group.getInteractions())
            );
        });

        YAxis yl = _barChart.getAxisLeft();
        yl.setDrawAxisLine(false);
        yl.setDrawLabels(false);
        yl.setDrawGridLines(false);
        yl.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis yr = _barChart.getAxisRight();
        yr.setDrawAxisLine(false);
        yr.setDrawLabels(false);
        yr.setDrawGridLines(false);
        yr.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        _barChart.setData(data);
        _barChart.setDrawBarShadow(false);
        _barChart.getDescription().setEnabled(false);

        _barChart.setPinchZoom(false);
        _barChart.setTouchEnabled(true);

        _barChart.setDrawGridBackground(false);

        _barChart.setVisibleXRangeMaximum(100f);
        _barChart.setVisibleXRangeMinimum(100f);

        _barChart.getLegend().setEnabled(false);
        _barChart.setFitBars(true);
        _barChart.notifyDataSetChanged();
        _barChart.invalidate();
        _barChart.moveViewToX(data.getXMax());
        _barChart.moveViewTo(data.getXMax(), data.getYMax(), yr.getAxisDependency());
        _barChart.animateY(500);

    }

    private void setupPieChart() {

        _pieChart.setUsePercentValues(true);
        _pieChart.getDescription().setEnabled(false);
        _pieChart.setDrawHoleEnabled(true);
        _pieChart.setHoleRadius(16f);
        _pieChart.setTransparentCircleRadius(20f);
        _pieChart.setRotationAngle(0f);
        _pieChart.setRotationEnabled(false);

        _pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                _selectedGroup = (Group) e.getData();
                setupBarChart((Group) e.getData());
            }
            @Override
            public void onNothingSelected() {
            }
        });

        List<PieEntry> entries = Stream.of(_groups)
                .filter(group -> group.getInteractions() > 0)
                .sorted((o1, o2) -> o1.getTitle().compareTo(o2.getTitle()))
                .map(group -> new PieEntry(group.getInteractions(), group.getTitle(), group))
                .collect(Collectors.toList());

        PieDataSet dataSet = new PieDataSet(entries, "This Year");
        dataSet.setSliceSpace(5f);
        dataSet.setSelectionShift(10f);
        dataSet.setColors(
                getResources().getColor(R.color.colorPieChart1),
                getResources().getColor(R.color.colorPieChart2),
                getResources().getColor(R.color.colorPieChart3),
                getResources().getColor(R.color.colorPieChart4)
        );

        PieData pieData = new PieData(dataSet);
        pieData.setValueFormatter(new PercentFormatter());
        pieData.setValueTextColor(Color.WHITE);
        pieData.setValueTextSize(14f);

        _pieChart.setData(pieData);
        _pieChart.notifyDataSetChanged();
        _pieChart.invalidate();

        _pieChart.animateY(500);

        _pieChart.getLegend().setEnabled(false);
//        Highlight highlight = new Highlight(0, 0, 0);
//        _pieChart.highlightValue(highlight);
        if (_selectedGroup == null && entries.size() > 0) {
            _selectedGroup = (Group) entries.get(0).getData();
        }
        setupBarChart(_selectedGroup);
    }
}