package dk.dtu.persdata.circlestrack.entities;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by knocker4 on 4/5/17.
 */

public class Person {
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String PHONE_NUMBERS = "phone_numbers";
    private static final String THUMBNAIL = "thumbnail";
    private static final String ENCOUNTERS = "encounters";

    private int _id;
    private String _name;
    private List<String> _phoneNumbers;
    private String _thumbnail;
    private int _encounters = 0;

    public Person(int id, String name, String phoneNumber) {
        this._id = id;
        this._name = name;
        this._phoneNumbers = new ArrayList<>();
        this._phoneNumbers.add(phoneNumber);
    }
    public Person(int id, String name, String phoneNumber, String thumbnail) {
        this._id = id;
        this._name = name;
        this._phoneNumbers = new ArrayList<>();
        this._phoneNumbers.add(phoneNumber);
        this._thumbnail = thumbnail;
    }


    public Person(String name, String phoneNumber) {
        this._name = name;
        this._phoneNumbers = new ArrayList<>();
        this._phoneNumbers.add(phoneNumber);
    }

    public Person(JSONObject jsonObject) {
        try {
            this._id = jsonObject.getInt(ID);
            this._name = jsonObject.getString(NAME);
            if (jsonObject.has(THUMBNAIL))
                this._thumbnail = jsonObject.getString(THUMBNAIL);
            this._encounters = jsonObject.getInt(ENCOUNTERS);
            this._phoneNumbers = new ArrayList<>();
            JSONArray array = jsonObject.getJSONArray(PHONE_NUMBERS);
            for (int i = 0; i < array.length(); i++) {
                this._phoneNumbers.add(array.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return _id;
    }

    public String getName() {
        return _name;
    }

    public List<String> getPhoneNumbers() {
        return _phoneNumbers;
    }

    public void addPhoneNumber(String phoneNumber) {
        _phoneNumbers.add(phoneNumber);
    }

    public int getEncounters() {
        return _encounters;
    }

    public void setEncounters(int encounters) {
        _encounters = encounters;
    }

    public void resetEncounters() { _encounters = 0; }

    public void incEncounters() {
        _encounters++;
    }

    public String getThumbnail() {
        return _thumbnail;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Stream.of(_phoneNumbers).forEach(number -> builder.append(number+", "));
        return _id + " : {" + _name + " : " + builder.toString() + " encounters: " + _encounters + " thumbnail: " + _thumbnail+"}";
    }

    public JSONObject toJson() {
        JSONObject personJson = new JSONObject();
        try {
            personJson.put(ID, _id);
            personJson.put(NAME, _name);
            personJson.put(THUMBNAIL, _thumbnail != null ? _thumbnail : "");
            JSONArray array = new JSONArray();
            if (_phoneNumbers != null)
                Stream.of(_phoneNumbers)
                      .forEach(array::put);
            personJson.put(PHONE_NUMBERS, array);
            personJson.put(ENCOUNTERS, _encounters);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return personJson;
    }

    @Override
    public int hashCode() {
        return _id;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Person && ((Person) obj)._id == this._id;
    }
}
