package dk.dtu.persdata.circlestrack.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.persdata.circlestrack.R;
import dk.dtu.persdata.circlestrack.entities.Group;
import dk.dtu.persdata.circlestrack.entities.Person;

/**
 * Created by knocker4 on 4/5/17.
 */

public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.GroupViewHolder> {

    public interface OnGroupSelectedListener {
        void onGroupClicked(Group group);
    }

    private List<Group> _groups;
    private List<Boolean> _selected;
    private OnGroupSelectedListener _listener;
    private Context _context;

    public GroupsAdapter(Context context, OnGroupSelectedListener listener) {
        _groups = new ArrayList<>();
        _context = context;
        _listener = listener;
    }

    public GroupsAdapter(Context context, List<Group> groups, OnGroupSelectedListener listener) {
        _groups = groups;
        _context = context;
        _listener = listener;
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group, parent, false);
        return new GroupViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position) {
        Group group = _groups.get(position);
        holder.name.setText(group.getTitle());
        holder.view.setOnClickListener(v -> _listener.onGroupClicked(group));
    }

    @Override
    public int getItemCount() {
        return _groups != null ? _groups.size() : 0;
    }

    class GroupViewHolder extends RecyclerView.ViewHolder{

        public View view;
        public TextView name;

        public GroupViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            name = (TextView) itemView.findViewById(R.id.name);
        }
    }

}
