package dk.dtu.persdata.circlestrack.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.telephony.PhoneNumberUtils;

import com.annimon.stream.Collectors;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import dk.dtu.persdata.circlestrack.R;
import dk.dtu.persdata.circlestrack.entities.Call;
import dk.dtu.persdata.circlestrack.entities.Group;
import dk.dtu.persdata.circlestrack.entities.Person;
import dk.dtu.persdata.circlestrack.entities.Sms;

/**
 * Created by knocker4 on 3/22/17.
 */

public class ConnectionsHistoryHelper {

    public enum SelectionType {
        DAY, WEEK, MONTH, YEAR
    }

    private static List<Person> _personsCache;
    private static List<Group> _groupsCache;
    private static SelectionType _currentSelection = SelectionType.YEAR;

    public static void updateGroups(Group group) {
        if (_groupsCache == null)
            _groupsCache = new ArrayList<>();
        if (_groupsCache.contains(group)) {
            _groupsCache.remove(group);
            _groupsCache.add(group);
        } else {
            _groupsCache.add(group);
        }
    }

    public static void saveGroups(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        Stream.of(_groupsCache)
              .forEach(dbHelper::updateGroup);
        dbHelper.close();
    }

    public static void initiateGroups(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        List<Group> groups = dbHelper.getAllGroups();
        if (groups.isEmpty()) {
            groups.add(new Group(context.getString(R.string.default_group_family)));
            groups.add(new Group(context.getString(R.string.default_group_friends)));
            groups.add(new Group(context.getString(R.string.default_group_work)));
            groups.add(new Group(context.getString(R.string.default_group_other)));
        }
        _groupsCache = groups;
        dbHelper.close();
    }

    public static Group getGroup(Context context, String groupName) {
        if (_groupsCache == null) {
            getGroups(context);
        }
        Optional<Group> groupOptional = Stream.of(_groupsCache)
                                                .filter(group -> group.getTitle().equals(groupName))
                                                .findFirst();
        return groupOptional.isPresent() ? groupOptional.get(): null;
    }

    public static List<Group> getGroups(Context context) {
        if (_groupsCache != null) {
            // TODO: 4/11/17 Invalidate cache after some time
            return _groupsCache;
        }
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        _groupsCache = dbHelper.getAllGroups();
        dbHelper.close();
        return _groupsCache;
    }

    public static void refreshGroups() {
        _groupsCache = Stream.of(_groupsCache)
                              .map(group -> {
                                  group.setPersons(
                                          Stream.of(group.getPersons())
                                                .map(person -> {
                                                    Optional<Person> groupPer = Stream.of(_personsCache)
                                                                              .filter(pC -> person.getId() == pC.getId())
                                                                              .findFirst();
                                                    return groupPer.isPresent() ? groupPer.get() : person;
                                                })
                                                .collect(Collectors.toList()));
                                  return group;
                              })
                              .collect(Collectors.toList());
    }

    public static void updateContacts(Context context, List<Person> persons) {
        _personsCache = persons;
    }

    public static List<Person> getContacts(Context context) {
        if (_personsCache != null) {
            // TODO: 4/7/17 Invalidate cache after some time
            return _personsCache;
        }
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions((Activity) context,
//                    new String[]{Manifest.permission.READ_CONTACTS},
//                    MainActivity.REQUEST_READ_CONTACTS);
            return new ArrayList<>();
        }
        Cursor cursor = context.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, null,
                null, null);
        int idI = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NAME_RAW_CONTACT_ID);
        int nameI = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int numberI = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        int photoI = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI);
        List<Person> persons = new ArrayList<>();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(idI);
            String name = cursor.getString(nameI);
            String number = cursor.getString(numberI);
            String thumbnail = cursor.getString(photoI);
            Optional existing = Stream.of(persons)
                                  .filter(person -> person.getId() == id)
                                  .findFirst();
            if (existing.isPresent()) {
                ((Person) existing.get()).addPhoneNumber(number);
            } else {
                persons.add(new Person(id, name, number, thumbnail));
            }
        }
        _personsCache = persons;
        cursor.close();
        return persons;
    }

    public static void resetEncounters() {
        if (_personsCache != null)
            Stream.of(_personsCache)
                  .forEach(Person::resetEncounters);
    }

    public static List<Sms> getSMSDetails(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions((Activity) context,
//                    new String[]{Manifest.permission.READ_SMS},
//                    MainActivity.REQUEST_READ_SMS_LOG);
            return new ArrayList<>();
        }

        String[] selectionArgs = getSelectionArguments();
        Cursor cursor = context.getContentResolver().query(
                Telephony.Sms.CONTENT_URI,
                null,
                CallLog.Calls.DATE+">=?", selectionArgs,
                null);
        int numberI = cursor.getColumnIndex(Telephony.Sms.ADDRESS);
        int dateI = cursor.getColumnIndex(Telephony.Sms.DATE);
        int typeI = cursor.getColumnIndex(Telephony.Sms.TYPE);
        List<Sms> smsList = new ArrayList<>();
        while (cursor.moveToNext()) {
            String number = cursor.getString(numberI);
            Date date = new Date(Long.valueOf(cursor.getString(dateI)));
            int type = Integer.parseInt(cursor.getString(typeI));
            switch (type) {
                case Telephony.Sms.MESSAGE_TYPE_INBOX:
                case Telephony.Sms.MESSAGE_TYPE_SENT:
                    smsList.add(new Sms(number, type, date));
                    break;
            }
        }
        cursor.close();
        Stream.of(smsList)
                .forEach(sms -> {
                    Optional<Person> contacted = Stream.of(_personsCache)
                            .filter(person -> Stream.of(person.getPhoneNumbers())
                                    .filter(number -> sms.getPhoneNumber().equals(number))
                                    .findFirst()
                                    .isPresent())
                            .findFirst();
                    if (contacted.isPresent())
                        contacted.get().incEncounters();
                });
        return smsList;
    }

    public static void setSelectionType(SelectionType selectionType) { _currentSelection = selectionType; }

    private static String[] getSelectionArguments() {
        List<String> selectionArgs = new ArrayList<>();
        switch (_currentSelection) {
            case DAY: {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                selectionArgs.add(calendar.getTime().getTime() + "");
                break;
//                return new String[]{calendar.getTime().getTime() + ""};
            }
            case WEEK: {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
                selectionArgs.add(calendar.getTime().getTime() + "");
                break;
//                return new String[]{calendar.getTime().getTime()+""};
            }
            case MONTH: {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                selectionArgs.add(calendar.getTime().getTime() + "");
                break;
//                return new String[]{calendar.getTime().getTime()+""};
            }
            case YEAR: {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MONTH, 0);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                selectionArgs.add(calendar.getTime().getTime() + "");
                break;
//                return new String[]{calendar.getTime().getTime()+""};
            }
        }
        String[] result = new String[selectionArgs.size()];
        return selectionArgs.toArray(result);
    }

    public static List<Call> getCallHistory(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions((Activity) context,
//                    new String[]{Manifest.permission.READ_CALL_LOG},
//                    MainActivity.REQUEST_READ_CALL_LOG);
            return new ArrayList<>();
        }
        String[] selectionArgs = getSelectionArguments();
        Cursor managedCursor = context.getContentResolver().query(
                CallLog.Calls.CONTENT_URI,
                null,
                CallLog.Calls.DATE+">=?", selectionArgs,
                null);
        int number = managedCursor.getColumnIndex( CallLog.Calls.NUMBER );
        int type = managedCursor.getColumnIndex( CallLog.Calls.TYPE );
        int date = managedCursor.getColumnIndex( CallLog.Calls.DATE);
        List<Call> calls = new ArrayList<>();
        while ( managedCursor.moveToNext() ) {
            String phNumber = managedCursor.getString( number );
            int callType = Integer.parseInt(managedCursor.getString( type ));
            Date callDate = new Date(Long.valueOf(managedCursor.getString( date )));
            switch( callType ) {
                case CallLog.Calls.OUTGOING_TYPE:
                case CallLog.Calls.INCOMING_TYPE: {
                    calls.add(new Call(phNumber, callType, callDate));
                    break;
                }
            }
        }
        managedCursor.close();
        Stream.of(calls)
                .forEach(call -> {
                    Optional<Person> contacted = Stream.of(_personsCache)
                            .filter(person -> Stream.of(person.getPhoneNumbers())
                                    .filter(num -> PhoneNumberUtils.compare(call.getPhoneNumber(), num))
                                    .findFirst()
                                    .isPresent())
                            .findFirst();
                    if (contacted.isPresent())
                        contacted.get().incEncounters();
                });
        return calls;
    }

}
