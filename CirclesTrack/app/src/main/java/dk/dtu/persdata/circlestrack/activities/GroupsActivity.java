package dk.dtu.persdata.circlestrack.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.annimon.stream.Collectors;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.persdata.circlestrack.adapters.GroupsAdapter;
import dk.dtu.persdata.circlestrack.adapters.PersonAdapter;
import dk.dtu.persdata.circlestrack.R;
import dk.dtu.persdata.circlestrack.entities.Group;
import dk.dtu.persdata.circlestrack.entities.Person;
import dk.dtu.persdata.circlestrack.utils.ConnectionsHistoryHelper;

public class GroupsActivity extends AppCompatActivity implements GroupsAdapter.OnGroupSelectedListener{

    private static final String DISPLAY_BACK = "DISPLAY_BACK";

    private List<Group> _groups;
    private List<Person> _persons;
    private List<Person> _chosenPeople;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);

        if (getSupportActionBar() != null && getIntent().getBooleanExtra(DISPLAY_BACK, false)) {
            getSupportActionBar().setTitle(R.string.activity_title_settings);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        _groups = ConnectionsHistoryHelper.getGroups(this);
        _persons = ConnectionsHistoryHelper.getContacts(this);

        List<Group> sortedGroups =
        Stream.of(_groups)
                .sorted((g1, g2) -> g1.getTitle().compareTo(g2.getTitle()))
                .collect(Collectors.toList());

        RecyclerView groupsContainer = (RecyclerView) findViewById(R.id.groups_container);
        groupsContainer.setLayoutManager(new LinearLayoutManager(this));
        GroupsAdapter adapter = new GroupsAdapter(this, sortedGroups, this);
        groupsContainer.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_activity_groups, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_save: {
                List<Person> chosen = Stream.of(_groups)
                                            .filter(group -> !(group.getTitle().equals(getString(R.string.default_group_other))))
                                            .map(Group::getPersons)
                                            .flatMap(Stream::of)
                                            .collect(Collectors.toList());
                List<Person> others = new ArrayList<>();
                Stream.of(_persons)
                      .filter(person -> {
                          Optional<Person> op = Stream.of(chosen)
                                                      .filter(chosenPerson -> person.getId() == chosenPerson.getId())
                                                      .findFirst();
                          return !(op.isPresent());
                      })
                      .forEach(others::add);

                ConnectionsHistoryHelper.updateGroups(new Group(getString(R.string.default_group_other), others));
                ConnectionsHistoryHelper.saveGroups(this);
                setResult(RESULT_OK);
                finish();
                break;
            }
            case android.R.id.home: {
                finish();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGroupClicked(Group group) {
        startActivity(EditGroupActivity.getActivityIntent(this, group));
    }

    public static Intent getActivityIntent(Context context, boolean displayBack) {
        Intent intent = new Intent(context, GroupsActivity.class);
        intent.putExtra(DISPLAY_BACK, displayBack);
        return intent;
    }
}