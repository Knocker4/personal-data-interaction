package dk.dtu.persdata.circlestrack.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;

import dk.dtu.persdata.circlestrack.R;
import dk.dtu.persdata.circlestrack.adapters.PersonAdapter;
import dk.dtu.persdata.circlestrack.entities.Group;
import dk.dtu.persdata.circlestrack.entities.Person;
import dk.dtu.persdata.circlestrack.utils.ConnectionsHistoryHelper;

/**
 * Created by knocker4 on 4/11/17.
 */

public class EditGroupActivity extends AppCompatActivity implements PersonAdapter.OnPersonSelectedListener{

    private static final String EXTRA_GROUP_NAME = "EXTRA_GROUP_NAME";

    private String _groupName;
    private List<Person> _persons;
    private List<Person> _chosenPeople;
    private PersonAdapter _adapter;

    public static Intent getActivityIntent(Context context, Group group) {
        Intent intent = new Intent(context, EditGroupActivity.class);
        intent.putExtra(EXTRA_GROUP_NAME, group.getTitle());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);

        _groupName = getIntent().getStringExtra(EXTRA_GROUP_NAME);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(_groupName);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        _persons = ConnectionsHistoryHelper.getContacts(this);
        _chosenPeople = new ArrayList<>();

        List<Person> sortedPerson =
        Stream.of(_persons)
              .sorted((p1, p2) -> p1.getName().compareTo(p2.getName()))
              .collect(Collectors.toList());


        RecyclerView personsContainer = (RecyclerView) findViewById(R.id.persons_container);
        personsContainer.setLayoutManager(new LinearLayoutManager(this));
        _adapter = new PersonAdapter(this, sortedPerson, this);
        personsContainer.setAdapter(_adapter);

        Group group = ConnectionsHistoryHelper.getGroup(this, _groupName);
        if (group != null) {
            Stream.of(group.getPersons())
                    .forEach(person -> {
                        _adapter.switchSelected(person);
                        _chosenPeople.add(person);
                    });
            _adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_activity_groups, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_save: {
                ConnectionsHistoryHelper.updateGroups(new Group(_groupName, _chosenPeople));
                setResult(RESULT_OK);
                finish();
                break;
            }
            case android.R.id.home: {
                finish();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPersonSelected(Person person, boolean selected) {
        Log.d("GroupsActivity", "onPersonSelected: " + person.getName() + " selected: " + selected);
        if (!selected) {
            _chosenPeople.add(person);
        } else {
            _chosenPeople.remove(person);
        }
        _adapter.switchSelected(person);
        _adapter.notifyDataSetChanged();
    }


}
