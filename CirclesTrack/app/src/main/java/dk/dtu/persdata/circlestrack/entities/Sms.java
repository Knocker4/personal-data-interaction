package dk.dtu.persdata.circlestrack.entities;

import java.util.Date;

/**
 * Created by knocker4 on 4/5/17.
 */

public class Sms {

    private String _phoneNumber;
    private int _smsType;
    private Date _smsDate;

    public Sms(String phoneNumber, int smsType, Date smsDate) {
        _phoneNumber = phoneNumber;
        _smsType = smsType;
        _smsDate = smsDate;
    }

    public String getPhoneNumber() {
        return _phoneNumber;
    }

    public int getSmsType() {
        return _smsType;
    }

    public Date getSmsDate() {
        return _smsDate;
    }
}
