package dk.dtu.persdata.circlestrack.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.annimon.stream.Collectors;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dk.dtu.persdata.circlestrack.R;
import dk.dtu.persdata.circlestrack.entities.Person;

/**
 * Created by knocker4 on 4/5/17.
 */

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.PersonViewHolder> {

    public interface OnPersonSelectedListener {
        void onPersonSelected(Person person, boolean selected);
    }

    private List<Person> _persons;
    private List<Pair<Person, Boolean>> _personsPairs;
    private List<Boolean> _selected;
    private OnPersonSelectedListener _listener;
    private Context _context;

    public PersonAdapter(Context context, OnPersonSelectedListener listener) {
        _persons = new ArrayList<>();
        _personsPairs = new ArrayList<>();
        _context = context;
        _listener = listener;
    }

    public PersonAdapter(Context context, List<Person> persons, OnPersonSelectedListener listener) {
        _persons = persons;
        _personsPairs = Stream.of(persons)
                                .map(person -> (new Pair<>(person, false)))
                                .collect(Collectors.toList());
        _context = context;
        _listener = listener;
    }

    public void switchSelected(Person person) {
        int index = -1;
        Pair<Person, Boolean> pair = null;

        for (int i = 0; i < _personsPairs.size(); i++) {
            if (_personsPairs.get(i).first.equals(person)) {
                index = i;
                pair = _personsPairs.get(i);
                break;
            }
        }

        if (index > -1 && pair != null) {
            _personsPairs.set(
                    index,
                    new Pair<>(pair.first, !pair.second));
        }
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person, parent, false);
        return new PersonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        Person person = _persons.get(position);
        holder.name.setText(person.getName());
        if (person.getThumbnail() != null) {
            holder.image.setImageURI(Uri.parse(person.getThumbnail()));
        } else {
            holder.image.setImageDrawable(_context.getResources().getDrawable(R.drawable.ic_account_box_black_48dp));
        }
        holder.checkBox.setChecked(_personsPairs.get(position).second);
        holder.checkBox.setOnClickListener(v -> {
//            holder.selected = !holder.selected;
            _listener.onPersonSelected(_personsPairs.get(position).first, _personsPairs.get(position).second);
        });
        holder.view.setOnClickListener(v -> {
//            holder.selected = !holder.selected;
            _listener.onPersonSelected(_personsPairs.get(position).first, _personsPairs.get(position).second);
            holder.checkBox.setChecked(!_personsPairs.get(position).second);
        });
    }

    @Override
    public int getItemCount() {
        return _persons != null ? _persons.size() : 0;
    }

    class PersonViewHolder extends RecyclerView.ViewHolder{

        public View view;
        public ImageView image;
        public TextView name;
        public CheckBox checkBox;
        public boolean selected;

        public PersonViewHolder(View itemView) {
            super(itemView);
            selected = false;
            view = itemView;
            image = (ImageView) itemView.findViewById(R.id.photo);
            name = (TextView) itemView.findViewById(R.id.name);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
        }
    }

}
