package dk.dtu.persdata.oddoneout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static int turns = 0;
    private static int REQUEST_TASK = 999;
    private TextView text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (TextView) findViewById(R.id.textView);
        text.setText(R.string.desc_first);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_TASK && resultCode == RESULT_OK) {

            turns = (turns + 1) % 3;
            switch (turns) {
                case 1: {text.setText(R.string.desc_second); break;}
                case 2: {text.setText(R.string.desc_third); break;}
            }

        }
    }

    public void startTesting() {
        // Do something in response to button

    }
}
