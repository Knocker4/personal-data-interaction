package dk.dtu.persdata.responsetime.utils;

import android.content.Context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;

import static android.R.attr.data;

/**
 * Created by knocker4 on 2/15/17.
 */

public class CSVWriter {

    public static class CSVData {
        public static final String HEADER_LINE =
                "time," +
                "success\n";

        public int success;
        public long time;


        public CSVData(long time, int success) {
            this.success = success;
            this.time = time;
        }

        @Override
        public String toString() {
            return  time + "," +
                    success + "\n";
        }
    }

    private Context _context;
    private String _path;

    public CSVWriter(Context context, String path) {
        _context = context;
        _path = path;
        File file = new File(_path);
        if (!file.exists()) {
            (new File(_path.substring(0, _path.lastIndexOf("/")))).mkdir();
            write(CSVData.HEADER_LINE);
        }
    }

    public void write(String line) {
        try {
            FileWriter fw = null;
            fw = new FileWriter(_path, false);
//            OutputStreamWriter osw = new OutputStreamWriter(fos);
            fw.write(line);
            fw.flush();
            fw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void appendData(CSVData data) {
        try {
            FileWriter fw = new FileWriter(_path, true);
            fw.write(data.toString());
            fw.flush();
            fw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
