package dk.dtu.persdata.responsetime;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import dk.dtu.persdata.responsetime.utils.CSVWriter;

public class MainActivity extends AppCompatActivity {

    private static final int MAX_DELAY_TIME = 20;
    private static  final  int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 999;
    private static final String PATH_DATA_RELATIVE = "/ResponseTime/data.csv";

    private CSVWriter _csvWriter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }

        if (isExternalStorageWritable() && permissionCheck != PackageManager.PERMISSION_DENIED) {
            _csvWriter = new CSVWriter(
                    this,
                    Environment.getExternalStorageDirectory().getAbsolutePath() + PATH_DATA_RELATIVE);
        } else {
            Toast.makeText(
                    this,
                    "Cannot access external storage. Data would not be saved.",
                    Toast.LENGTH_SHORT)
                    .show();
        }

        TextView dialogButton = (TextView) findViewById(R.id.dialogButton);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final long delay = (long) (new Random().nextInt(MAX_DELAY_TIME)*100 + new Random().nextInt(2)*50);
                final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("How was it?")
                        .setMessage("Did you experience any lag?")
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (_csvWriter != null) {
                                    _csvWriter.appendData(new CSVWriter.CSVData(delay, 1));
                                }
                            }
                        })
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (_csvWriter != null) {
                                    _csvWriter.appendData(new CSVWriter.CSVData(delay, 0));
                                }
                            }
                        })
                        .create();
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.show();
                    }
                }, delay);
            }
        });
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    _csvWriter = new CSVWriter(
                            this,
                            Environment.getExternalStorageDirectory().getAbsolutePath() + PATH_DATA_RELATIVE);

                } else {
                }
                return;
            }
        }
    }



}
