package dk.dtu.persdata.oddoneout.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import dk.dtu.persdata.oddoneout.R;
import dk.dtu.persdata.oddoneout.adapters.CustomGridAdapter;
import dk.dtu.persdata.oddoneout.utils.CSVWriter;

public class TaskActivity extends AppCompatActivity {

    public static final String BUNDLE_KEY = "TaskActivity.key";
    public static final String RESULT_SUCCESS = "TaskActivity.SUCCESS";
    public static final String RESULT_TOTAL = "TaskActivity.TOTAL";
    public static final String RESULT_TIME = "TaskActivity.TIME";

    private static final int N = 25;
    private static final int MAX_TURNS = 10;
    private static final String PATH_DATA_RELATIVE = "/OddOneOut/data.csv";
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 990;

    private int _turns;
    private int _key;
    private int _success;
    private long[] _time;

    private long _prevTime;
    private CSVWriter _csvWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        _turns = 0;
        _success = 0;
        _time = new long[10];
        _key = getIntent().getIntExtra(BUNDLE_KEY, -1);

        _prevTime = System.currentTimeMillis();

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (isExternalStorageWritable() && permissionCheck != PackageManager.PERMISSION_DENIED) {
            _csvWriter = new CSVWriter(
                    this,
                    Environment.getExternalStorageDirectory().getAbsolutePath() + PATH_DATA_RELATIVE);
        } else {
            Toast.makeText(
                    this,
                    "Cannot access external storage. Data would not be saved.",
                    Toast.LENGTH_SHORT)
                 .show();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        GridView gridView = (GridView) findViewById(R.id.grid_container);
        final CustomGridAdapter adapter = new CustomGridAdapter(this);
        adapter.setList(randomizeValues(N));
        adapter.setListener(new CustomGridAdapter.OnArrowClickedListener() {
            @Override
            public void onArrowClicked(boolean value) {
//                Toast.makeText(TaskActivity.this, value ? "Nope" : "Cool", Toast.LENGTH_SHORT).show();
                _success += value ? 1 : 0;
                _time[_turns] = System.currentTimeMillis() - _prevTime;
                _turns++;
                if (_turns >= MAX_TURNS) {
                    if (_csvWriter != null)
                        _csvWriter.appendData(new CSVWriter.CSVData(
                            _key,
                            _success,
                            _time));
                    Intent resultData = new Intent();
                    resultData.putExtra(RESULT_SUCCESS, _success);
                    resultData.putExtra(RESULT_TOTAL, MAX_TURNS);
                    resultData.putExtra(RESULT_TIME, _time);
                    setResult(RESULT_OK, resultData);
                    finish();
                } else {
                    _prevTime = System.currentTimeMillis();
                    adapter.setList(randomizeValues(N));
                    adapter.notifyDataSetChanged();
                }
            }
        });
        gridView.setAdapter(adapter);

    }

    private List<Boolean> randomizeValues(int N) {
        List<Boolean> list = new ArrayList<>();
        for (int i=0; i < N; i++) {
            list.add(false);
        }
        list.set(new Random().nextInt(N), true);
        return list;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
}
