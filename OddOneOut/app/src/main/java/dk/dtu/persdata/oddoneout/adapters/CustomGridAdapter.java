package dk.dtu.persdata.oddoneout.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import dk.dtu.persdata.oddoneout.R;

/**
 * Created by knocker4 on 2/15/17.
 */

public class CustomGridAdapter extends BaseAdapter{

    public interface OnArrowClickedListener {
        void onArrowClicked(boolean value);
    }

    private LayoutInflater _inflater;
    private List<Boolean> _list;
    private Context _context;
    private OnArrowClickedListener _listener;

    public void setList(List<Boolean> list) {
        _list = list;
    }

    public void setListener(OnArrowClickedListener listener) {
        _listener = listener;
    }

    public CustomGridAdapter(Context context) {
        _context = context;
        _inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CustomGridAdapter(Context context, List<Boolean> list, OnArrowClickedListener listener) {
        _context = context;
        _list = list;
        _listener = listener;
        _inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return _list.size();
    }

    @Override
    public Object getItem(int i) {
        return _list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup parent) {
        ArrowViewHolder viewHolder;
        if (convertView == null) {
            convertView = _inflater.inflate(R.layout.item_arrow, parent, false);
            viewHolder = new ArrowViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ArrowViewHolder) convertView.getTag();
        }

        int drawableRes = _list.get(i) ? R.drawable.ic_arrow_back_black_48dp : R.drawable.ic_arrow_forward_black_48dp;
        viewHolder.imageView.setImageDrawable(_context.getResources().getDrawable(drawableRes));
        viewHolder.arrowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.onArrowClicked(_list.get(i));
            }
        });

        return convertView;
    }

    static class ArrowViewHolder {
        LinearLayout arrowLayout;
        ImageView imageView;

        public ArrowViewHolder(View view) {
            arrowLayout = (LinearLayout) view.findViewById(R.id.arrow_layout);
            imageView = (ImageView) view.findViewById(R.id.arrow_image);
        }

    }



}
