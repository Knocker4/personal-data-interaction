package dk.dtu.persdata.oddoneout.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import dk.dtu.persdata.oddoneout.R;

public class MainActivity extends AppCompatActivity {

    private static int turns = 0;
    private static int REQUEST_TASK = 999;
    private TextView text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (TextView) findViewById(R.id.textView);
        text.setText(R.string.desc_first);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_TASK && resultCode == RESULT_OK) {
            if (data != null) {
                long timeSum = 0;
                long[] time = data.getLongArrayExtra(TaskActivity.RESULT_TIME);
                for (int i = 0; i < time.length; i++) {
                    timeSum += time[i];
                }
                String message = "You succeeded " + data.getIntExtra(TaskActivity.RESULT_SUCCESS, 0) +
                                 " out of " + data.getIntExtra(TaskActivity.RESULT_TOTAL, 0) +
                                 " in " + ((float) timeSum) / 1000 + "s";
                AlertDialog alertDialog = new AlertDialog.Builder(this)
                                        .setTitle("Results")
                                        .setMessage(message)
                                        .setPositiveButton("Great!", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        })
                                        .create();
                alertDialog.show();
            }
            turns = (turns + 1) % 3;
            switch (turns) {
                case 0: {text.setText(R.string.desc_first); break;}
                case 1: {text.setText(R.string.desc_second); break;}
                case 2: {text.setText(R.string.desc_third); break;}
            }
        }
    }

    public void startTesting(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, TaskActivity.class);
        intent.putExtra(TaskActivity.BUNDLE_KEY, turns);
        startActivityForResult(
                intent,
                REQUEST_TASK
        );
    }
}
